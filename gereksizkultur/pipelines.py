# -*- coding: utf-8 -*-

import logging
import cloudinary
import cloudinary.uploader
import cloudinary.api
from scrapy.exceptions import DropItem
from pymongo import MongoClient

logger = logging.getLogger(__name__)
client = MongoClient('mongodb://python:python@ds121336.mlab.com:21336/gereksizkultur')
db = client.get_default_database()

cloudinary.config(
    cloud_name="stomger",
    api_key="152298493425972",
    api_secret="xr4fQtQkE60VTTdkF1XkMoO9il0"
)

class GereksizKulturPipeline(object):
    def __init__(self):
        self.post_seen = set()
        
    def process_item(self, item, spider):
        if item['source_url'] in self.post_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            try:
                cloud = cloudinary.uploader.upload(item['image_url'], folder='gereksizkultur')
                item['image_url'] = cloud['url']
            except:
                item['image_url'] = item['image_url']
                    
            if db.posts.find_one({"source_url": item['source_url']}):
                logger.warning('Already exists posts: {0}'.format(item['source_url']))
                raise DropItem("Duplicate item found: %s" % item['source_url'])

            if str(db.posts.insert_one(item)):
                logger.info('Successful download url: {0}'.format(item['source_url']))
                self.post_seen.add(item['source_url'])
                return item
            else:
                logger.error('Error download url: {0}'.format(item['source_url']))
                raise DropItem("Error item: %s" % item['source_url'])
