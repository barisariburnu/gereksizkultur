# -*- coding: utf-8 -*-

import sys
import uuid
import logging
from datetime import datetime
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from gereksizkultur.items import GereksizKulturItem

logger = logging.getLogger(__name__)

reload(sys)
sys.setdefaultencoding('utf-8')


class MilyarBilgi(CrawlSpider):
    name = 'milyarbilgi'
    allowed_domains = ['1milyarbilgi.com']
    start_urls = ['http://1milyarbilgi.com']
    
    rules = [
        Rule(
            LinkExtractor(allow=[
                '\/',
                '\/kategori\/[a-z0-9-.]*'
            ], restrict_xpaths='//li[@class="next"]/a'),
            callback='parse_item',
            follow=True
        )
    ]

    def parse_item(self, response):
        urls = response.css('div.title-container > a::attr(href)').extract()
        
        for url in urls:
            url = '{base}{url}'.format(base='http://www.1milyarbilgi.com', url=url)
            yield Request(url, callback=self.parse_details, dont_filter=True)
            
    def parse_details(self, response):
        item = GereksizKulturItem()
        item['_id'] = str(uuid.uuid4())
        item['title'] = response.css('div.title-container > div.news-header > h1.title::text').extract_first()
        
        post = response.xpath('//div[@class="news-detail-text"]').extract_first()
        item['post'] = post.split('<div id="links" class="links hidden"></div>')[0]
        
        item['summary'] = post
        
        category = response.css('div.breadcrumb > span > a::text').extract_first()
        item['category'] = 'Kültür ve Sanat' if category == 'Kültür-Sanat' else category 
        
        tags = response.css('div.news-detail-tags > li.tags > a::text').extract()
        tags.extend(['gereksiz kültür', 'bilgi'])
        item['tags'] = [item.strip().lower() for item in tags  if str(item).strip() != '' ]
        
        item['created'] = datetime.now()
        item['source'] = "1milyarbilgi.com"
        item['source_url'] = response.url
        item['image_url'] = response.xpath('//meta[@property="og:image"]/@content').extract()[0]
        item['is_added'] = "0"
        
        return item