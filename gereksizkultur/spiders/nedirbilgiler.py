# -*- coding: utf-8 -*-

import sys
import uuid
import logging
from datetime import datetime
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from gereksizkultur.items import GereksizKulturItem

logger = logging.getLogger(__name__)

reload(sys)
sys.setdefaultencoding('utf-8')


class NedirBilgiler(CrawlSpider):
    name = 'nedirbilgiler'
    allowed_domains = ['www.nedirbilgiler.com']
    start_urls = ['http://www.nedirbilgiler.com']

    rules = [
        Rule(
            LinkExtractor(allow=[
                '\/category\/is',
                '\/category\/saglik',
                '\/category\/teknoloji',
                '\/category\/eniyi',
                '\/category\/egitim',
                '\/category\/din'
                '\/category\/[a-z0-9-.]*\/page\/d+'
            ]),
            callback='parse_item',
            follow=True
        )
    ]

    def parse_item(self, response):
        urls = response.css('div.single-post-wrapper > a::attr(href)')
        
        for url in urls:
            yield Request(url.extract(), callback=self.parse_post, dont_filter=True)

    def parse_post(self, response):
        item = GereksizKulturItem()
        item['_id'] = str(uuid.uuid4())
        item['title'] = response.css('h1.entry-title::text').extract_first()
        
        post_first = response.css('div.entry-content').extract_first().split('<p><script async')[0].replace('\n', '')
        post_last = response.css('div.entry-content').extract_first().split('</script></p>')[1].replace('\n', '')
        item['post'] = post_first + post_last
        
        item['summary'] = item['post']
        item['category'] = 'Genel'
        item['tags'] = ['gereksiz kültür', 'bilgi', 'genel']
        item['created'] = datetime.now()
        item['source'] = "nedirbilgiler.com"
        item['source_url'] = response.url
        item['image_url'] = response.css('div.single-post-image > figure > img::attr(src)').extract_first()
        item['is_added'] = "0"
        
        return item
