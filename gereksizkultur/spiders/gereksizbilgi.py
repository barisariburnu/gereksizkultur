# -*- coding: utf-8 -*-

import sys
import uuid
import logging
from datetime import datetime
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from gereksizkultur.items import GereksizKulturItem

logger = logging.getLogger(__name__)

reload(sys)
sys.setdefaultencoding('utf-8')


class GereksizBilgi(CrawlSpider):
    name = 'gereksizbilgi'
    allowed_domains = ['www.gereksizbilgi.com']
    start_urls = ['http://www.gereksizbilgi.com']

    rules = [
        Rule(
            LinkExtractor(allow=[
                '\/category\/insanlar',
                '\/category\/bilim',
                '\/category\/hayvanlar',
                '\/category\/dunya',
                '\/category\/tarih',
                '\/category\/pratik-bilgiler',
                '\/category\/cocuklar',
                '\/category\/[a-z0-9-.]*\/page\/d+'
            ]),
            callback='parse_item',
            follow=True
        )
    ]

    def parse_item(self, response):
        urls = response.xpath('//a[@class="post-url post-title"]/@href')
        
        for url in urls:
            yield Request(url.extract(), callback=self.parse_post, dont_filter=True)

    def parse_post(self, response):
        item = GereksizKulturItem()
        item['_id'] = str(uuid.uuid4())
        item['title'] = response.css('h1.single-post-title > span.post-title::text').extract_first()
        
        post = response.css('div.entry-content > p').extract()
        item['post'] = ''.join(post)
        
        item['summary'] = item['post']
        item['category'] = response.css('div.term-badges')[0].css('span.term-badge > a::text').extract_first()
        
        tags = response.xpath('//meta[@name="keywords"]/@content').extract()[0].split(',')
        tags.extend(['gereksiz kültür', 'bilgi'])
        item['tags'] = [item.strip().lower() for item in tags  if str(item).strip() != '' ]
        
        created = response.xpath('//meta[@property="article:published_time"]/@content').extract()[0]
        year, month, day = created.split('T')[0].split('-')
        created = '{day}/{month}/{year}'.format(day=day, month=month, year=year)
        item['created'] = datetime.strptime(created, '%d/%m/%Y')
        
        item['source'] = "gereksizbilgi.com"
        item['source_url'] = response.url
        item['image_url'] = response.xpath('//div[@class="post-header post-tp-11-header bs-lazy wfi"]/@data-src').extract()[0]
        item['is_added'] = "0"
        
        return item
