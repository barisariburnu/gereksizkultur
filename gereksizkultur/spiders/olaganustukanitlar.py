# -*- coding: utf-8 -*-

import sys
import uuid
import logging
from datetime import datetime
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request
from gereksizkultur.items import GereksizKulturItem

logger = logging.getLogger(__name__)

reload(sys)
sys.setdefaultencoding('utf-8')


class OlaganustuKanitlar(CrawlSpider):
    name = 'olaganustukanitlar'
    allowed_domains = ['www.olaganustukanitlar.com']
    start_urls = ['http://www.olaganustukanitlar.com']

    rules = [
        Rule(
            LinkExtractor(allow=[
                '\/category\/bilim-ve-teknoloji',
                '\/category\/ilginc-bilgiler',
                '\/category\/tarih',
                '\/category\/kultur-ve-sanat',
                '\/category\/canlilar',
                '\/category\/[a-z0-9-.]*\/page\/d+'
            ]),
            callback='parse_item',
            follow=True
        )
    ]

    def parse_item(self, response):
        urls = response.css('main.site-main > article > header > h2.entry-title > a::attr(href)')
        
        for url in urls:
            yield Request(url.extract(), callback=self.parse_post, dont_filter=True)

    def parse_post(self, response):
        item = GereksizKulturItem()
        item['_id'] = str(uuid.uuid4())
        item['title'] = response.css('div.page-header > h1.page-title::text').extract_first()
        
        post = response.css('div.entry-content').extract_first().split('<!-- Begin Yuzo -->')[0].split('<div class="entry-content clearfix">')[1].replace('\n', '').replace('\t', '')
        item['post'] = post
        
        item['summary'] = response.xpath('//meta[@name="description"]/@content').extract()[0]
        item['category'] = 'Genel'
        
        tags = response.xpath('//meta[@name="keywords"]/@content').extract()[0].split(',')
        tags.extend(['gereksiz kültür', 'bilgi'])
        item['tags'] = [item.strip().lower() for item in tags  if str(item).strip() != '' ]
        
        item['created'] = datetime.now()
        item['source'] = "olaganustukanitlar.com"
        item['source_url'] = response.url
        item['image_url'] = response.css('div.entry-content > p > img::attr(src)').extract_first()
        item['is_added'] = "0"
        
        return item
