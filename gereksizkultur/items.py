# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class GereksizKulturItem(scrapy.Item):
    _id = scrapy.Field()
    title = scrapy.Field()
    summary = scrapy.Field()
    post = scrapy.Field()
    category = scrapy.Field()
    tags = scrapy.Field()
    created = scrapy.Field()
    source = scrapy.Field()
    source_url = scrapy.Field()
    image_url = scrapy.Field()
    is_added = scrapy.Field()
