#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import re
import requests
from PIL import Image
from io import BytesIO
from slugify import slugify
from bs4 import BeautifulSoup
from pymongo import MongoClient
from mysql.connector import connection
from datetime import datetime, timedelta

BASE_FILE = 'C:\\xampp\\htdocs\\varient\\uploads'
DOMAIN = 'http://localhost/varient/'

client = MongoClient('mongodb://python:python@ds121336.mlab.com:21336/gereksizkultur')
db = client.get_default_database()

posts_insert = 'INSERT INTO posts(title, title_slug, keywords, summary, content, category_id, ' \
               'subcategory_id, image_big, image_default, image_slider, image_mid, image_small, need_auth, ' \
               'is_slider, slider_order, is_featured, featured_order, is_recommended, is_breaking, ' \
               'visibility, user_id, status, created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ' \
               '%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

tags_insert = 'INSERT INTO tags(post_id, tag, tag_slug) VALUES (%s, %s, %s)'
tags_select = 'SELECT * FROM tags WHERE post_id = %s AND tag_slug = %s'


def save_post():
    collection = db['posts']
    results = collection.find({"is_added": "0"}).sort([('created', 1)]).limit(5)

    for item in results:
        conn = connection.MySQLConnection(host="localhost", user="root", database="varient")
        cursor = conn.cursor()

        title = item['title'].strip()
        slug = slugify(item['title'], to_lower=True)
        tags = ",".join(item['tags'])
        summary = re.sub("<.*?>", "", item['summary'])
        post = item['post']
        category = choose_category(item['category'])
        created = (datetime.now() + timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')

        soup = BeautifulSoup(post, 'lxml')

        # Post içindeki resimleri local bilgisayara indirip içeriği günceller
        try:
            count = 1
            for image in soup.findAll("img"):
                name = '{slug}-{count}'.format(slug=slug, count=count)
                path = save_image(image['src'], name)
                image['src'] = path
                count += 1
            post = str(soup)
        except Exception as ex:
            print(ex.message)
            break

        # Cover resmini local bilgisayara indirir. Boyutlandırma işlemini yapar.
        try:
            path_list = save_cover(item['image_url'], slug)
        except Exception as ex:
            print(ex.message)
            break

        try:
            post_args = (
                title,
                slug,
                tags,
                summary,
                post,
                category[0],
                category[1],
                path_list[0],
                path_list[1],
                path_list[2],
                path_list[3],
                path_list[4],
                0, 1, 1, 1, 1, 1, 0, 1, 1, 1,
                created
            )

            cursor.execute(posts_insert, post_args)

            if not cursor.lastrowid:
                continue

            for tag in item['tags']:
                args = (cursor.lastrowid, slugify(tag, to_lower=True))
                cursor.execute(tags_select, args)
                result = cursor.fetchall()

                if len(result) > 0:
                    continue

                tag_args = (
                    cursor.lastrowid,
                    tag,
                    slugify(tag, to_lower=True)
                )
                cursor.execute(tags_insert, tag_args)

            conn.commit()
            collection.update_one({'_id': item['_id']}, {"$set": {"is_added": 1}})
            print('Successful: ' + item['source_url'])
        except Exception as err:
            conn.rollback()
            print(err)
        finally:
            cursor.close()
            conn.close()


def choose_category(category):
    category = category.lower().strip()

    if category in ['sağlık']:
        return [1, 5]
    elif category in ['spor', 'yaşam']:
        return [1, 6]
    elif category == 'doğa':
        return [1, 7]
    elif category == 'ilaç rehberi':
        return [1, 8]
    elif category == 'mutfak':
        return [1, 9]
    elif category == 'rüya tabirleri':
        return [1, 10]
    elif category in ['bilim', 'uzay']:
        return [2, 11]
    elif category == 'teknoloji':
        return [2, 12]
    elif category == 'tarih':
        return [4, 13]
    elif category in ['kültür ve sanat', 'hızlı bilgi', 'atasözleri', 'insanlar', 'hayvanlar', 'dünya',
                      'pratik bilgiler', 'genel', 'çocuklar', 'bitkiler', 'aletler', 'arabalar']:
        return [3, 15]
    elif category == 'biyografi':
        return [3, 16]
    elif category == 'eğitim':
        return [3, 17]
    elif category == 'sözlük':
        return [3, 18]


def save_cover(url, name):
    data = requests.get(url).content
    im = Image.open(BytesIO(data))

    sizes = [(im.width, im.height), (750, im.height), (600, 460), (380, 240), [140, 98]]
    urls = []
    for size in sizes:
        img = im.resize(size, Image.ANTIALIAS)
        filename = '{NAME}-{WIDTH}x{HEIGHT}'.format(NAME=name, WIDTH=size[0], HEIGHT=size[1])
        img.save(os.path.join(BASE_FILE, 'images', filename), "PNG")
        urls.append('uploads/images/' + filename)

    return urls


def save_image(url, name):
    data = requests.get(url).content
    im = Image.open(BytesIO(data))
    im.save(os.path.join(BASE_FILE, 'files', name), "PNG")
    return DOMAIN + 'uploads/files/' + name


save_post()

# results = collection.update_one({}, {"$set": {"is_added": '0'}})
# results = collection.update_one({"source_url": item["source_url"]}, {"$set": {"tags": tags}})

# try:
#     collection = db['posts']
#     result = collection.update({}, {"$set": {"is_added": '0'}}, multi=True)
#     print(result)

# except:
#     print('Kayıtlar getirilirken hata oluştu!')
